/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_product_image表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|product_image_id    |INT(10)             |false |true    |NULL    |
|product_id          |INT(10)             |false |false   |NULL    |
|image               |VARCHAR(255)        |true  |false   |NULL    |
|sort_order          |INT(10)             |false |false   |0|
+--------------------+--------------------+------+--------+--------+--------

 */
public class ProductImage extends Model<ProductImage>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2450351483282245035L;
	/**
	 * 用于查询操作
	 */
	public static final ProductImage ME = new ProductImage();
	
}