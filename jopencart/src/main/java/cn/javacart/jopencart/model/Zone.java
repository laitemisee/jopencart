/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_zone表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|zone_id             |INT(10)             |false |true    |NULL    |
|country_id          |INT(10)             |false |false   |NULL    |
|name                |VARCHAR(128)        |false |false   |NULL    |
|code                |VARCHAR(32)         |false |false   |NULL    |
|status              |BIT(0)              |false |false   |1|
+--------------------+--------------------+------+--------+--------+--------

 */
public class Zone extends Model<Zone>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2497151483282249715L;
	/**
	 * 用于查询操作
	 */
	public static final Zone ME = new Zone();
	
}