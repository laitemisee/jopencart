/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_review表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|review_id           |INT(10)             |false |true    |NULL    |
|product_id          |INT(10)             |false |false   |NULL    |
|customer_id         |INT(10)             |false |false   |NULL    |
|author              |VARCHAR(64)         |false |false   |NULL    |
|text                |TEXT(65535)         |false |false   |NULL    |
|rating              |INT(10)             |false |false   |NULL    |
|status              |BIT(0)              |false |false   |0|
|date_added          |DATETIME(19)        |false |false   |NULL    |
|date_modified       |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Review extends Model<Review>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2474141483282247414L;
	/**
	 * 用于查询操作
	 */
	public static final Review ME = new Review();
	
}