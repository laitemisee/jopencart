/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_order_option表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|order_option_id     |INT(10)             |false |true    |NULL    |
|order_id            |INT(10)             |false |false   |NULL    |
|order_product_id    |INT(10)             |false |false   |NULL    |
|product_option_id   |INT(10)             |false |false   |NULL    |
|product_option_value_id|INT(10)             |false |false   |0|
|name                |VARCHAR(255)        |false |false   |NULL    |
|value               |TEXT(65535)         |false |false   |NULL    |
|type                |VARCHAR(32)         |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class OrderOption extends Model<OrderOption>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2434541483282243454L;
	/**
	 * 用于查询操作
	 */
	public static final OrderOption ME = new OrderOption();
	
}