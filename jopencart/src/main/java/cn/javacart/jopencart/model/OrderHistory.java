/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_order_history表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|order_history_id    |INT(10)             |false |true    |NULL    |
|order_id            |INT(10)             |false |false   |NULL    |
|order_status_id     |INT(10)             |false |false   |NULL    |
|notify              |BIT(0)              |false |false   |0|
|comment             |TEXT(65535)         |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class OrderHistory extends Model<OrderHistory>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2433201483282243320L;
	/**
	 * 用于查询操作
	 */
	public static final OrderHistory ME = new OrderHistory();
	
}