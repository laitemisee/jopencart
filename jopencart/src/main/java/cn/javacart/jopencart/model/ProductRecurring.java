/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_product_recurring表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|product_id          |INT(10)             |false |true    |NULL    |
|recurring_id        |INT(10)             |false |true    |NULL    |
|customer_group_id   |INT(10)             |false |true    |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class ProductRecurring extends Model<ProductRecurring>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2454221483282245422L;
	/**
	 * 用于查询操作
	 */
	public static final ProductRecurring ME = new ProductRecurring();
	
}