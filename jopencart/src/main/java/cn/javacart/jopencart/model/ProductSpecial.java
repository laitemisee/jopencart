/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_product_special表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|product_special_id  |INT(10)             |false |true    |NULL    |
|product_id          |INT(10)             |false |false   |NULL    |
|customer_group_id   |INT(10)             |false |false   |NULL    |
|priority            |INT(10)             |false |false   |1|
|price               |DECIMAL(15)         |false |false   |0.0000|
|date_start          |DATE(10)            |false |false   |0000-00-00|
|date_end            |DATE(10)            |false |false   |0000-00-00|
+--------------------+--------------------+------+--------+--------+--------

 */
public class ProductSpecial extends Model<ProductSpecial>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2458101483282245810L;
	/**
	 * 用于查询操作
	 */
	public static final ProductSpecial ME = new ProductSpecial();
	
}