/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_length_class_description表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|length_class_id     |INT(10)             |false |true    |NULL    |
|language_id         |INT(10)             |false |true    |NULL    |
|title               |VARCHAR(32)         |false |false   |NULL    |
|unit                |VARCHAR(4)          |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class LengthClassDescription extends Model<LengthClassDescription>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2415601483282241560L;
	/**
	 * 用于查询操作
	 */
	public static final LengthClassDescription ME = new LengthClassDescription();
	
}