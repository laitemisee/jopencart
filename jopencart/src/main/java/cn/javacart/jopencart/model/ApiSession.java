/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_api_session表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|api_session_id      |INT(10)             |false |true    |NULL    |
|api_id              |INT(10)             |false |false   |NULL    |
|token               |VARCHAR(32)         |false |false   |NULL    |
|session_id          |VARCHAR(32)         |false |false   |NULL    |
|session_name        |VARCHAR(32)         |false |false   |NULL    |
|ip                  |VARCHAR(40)         |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
|date_modified       |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class ApiSession extends Model<ApiSession>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2341641483282234164L;
	/**
	 * 用于查询操作
	 */
	public static final ApiSession ME = new ApiSession();
	
}