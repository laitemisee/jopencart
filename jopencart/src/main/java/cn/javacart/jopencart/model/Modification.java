/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_modification表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|modification_id     |INT(10)             |false |true    |NULL    |
|name                |VARCHAR(64)         |false |false   |NULL    |
|code                |VARCHAR(64)         |false |false   |NULL    |
|author              |VARCHAR(64)         |false |false   |NULL    |
|version             |VARCHAR(32)         |false |false   |NULL    |
|link                |VARCHAR(255)        |false |false   |NULL    |
|xml                 |MEDIUMTEXT(16777215)|false |false   |NULL    |
|status              |BIT(0)              |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Modification extends Model<Modification>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2422171483282242217L;
	/**
	 * 用于查询操作
	 */
	public static final Modification ME = new Modification();
	
}