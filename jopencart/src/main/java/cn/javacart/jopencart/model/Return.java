/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_return表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|return_id           |INT(10)             |false |true    |NULL    |
|order_id            |INT(10)             |false |false   |NULL    |
|product_id          |INT(10)             |false |false   |NULL    |
|customer_id         |INT(10)             |false |false   |NULL    |
|firstname           |VARCHAR(32)         |false |false   |NULL    |
|lastname            |VARCHAR(32)         |false |false   |NULL    |
|email               |VARCHAR(96)         |false |false   |NULL    |
|telephone           |VARCHAR(32)         |false |false   |NULL    |
|product             |VARCHAR(255)        |false |false   |NULL    |
|model               |VARCHAR(64)         |false |false   |NULL    |
|quantity            |INT(10)             |false |false   |NULL    |
|opened              |BIT(0)              |false |false   |NULL    |
|return_reason_id    |INT(10)             |false |false   |NULL    |
|return_action_id    |INT(10)             |false |false   |NULL    |
|return_status_id    |INT(10)             |false |false   |NULL    |
|comment             |TEXT(65535)         |true  |false   |NULL    |
|date_ordered        |DATE(10)            |false |false   |0000-00-00|
|date_added          |DATETIME(19)        |false |false   |NULL    |
|date_modified       |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Return extends Model<Return>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2467621483282246762L;
	/**
	 * 用于查询操作
	 */
	public static final Return ME = new Return();
	
}