package cn.javacart.jopencart.interceptor;

import java.util.Map;

import javax.servlet.http.HttpSession;

import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartInterceptor;
import cn.javacart.jopencart.library.LanguageService;
import cn.javacart.jopencart.library.RequestService;
import cn.javacart.jopencart.library.SessionConfigService;
import cn.javacart.jopencart.library.SessionService;
import cn.javacart.jopencart.library.cart.CustomerService;
import cn.javacart.jopencart.model.Address;

import com.jfinal.aop.Duang;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;

/**
 * 初始化
 * @author farmer
 *
 */
public class JOpencartInitConfigInterceptor extends JOpencartInterceptor{

	@Override
	public void doIntercept(Invocation inv, JOpencartController controller) {
		if(controller == null){
			inv.invoke();
			return;
		}
		HttpSession session = controller.getSession();
		RequestService.init(controller.getRequest());//设置Request到线程变量
		SessionService.init(session);		  //设置Seesion到线程变量
		SessionConfigService.init(session);	//初始化，设置session
		Map<String, Object> configMap = SessionConfigService.getConfigMap();	//从session中获取配置信息
		if(configMap == null){	//首次进入系统设置
			String language =  null;//从Session中读取语言
			if(StrKit.notBlank((String)SessionConfigService.get("language"))){
				language = SessionConfigService.get("language");
			}else if(StrKit.notBlank((String)controller.getCookie("language"))){
				language = controller.getCookie("language");
			}
			if(Db.queryLong("select count(1) from  joc_language t where t.code = ? and t.status = ?",language,1) == 0){
				language =  (String)config.get("config_language");				
			}
			SessionConfigService.set("config_language_id", Db.queryInt("select language_id from joc_language t where t.code =  ?", language));
			SessionConfigService.set("config_language", language);
			controller.setCookie("language", language, 60 * 60 * 24 * 30);
			controller.setSessionAttr("language", language);
			LanguageService.getInstance().loadSession("/catalog/language/"+language+"/"+language+".php");	//初始化加载语言
			//设置回话的Customer
			SessionConfigService.set("customer", Duang.duang(CustomerService.class));
			//设置
			String currency = null;
			if(StrKit.notBlank((String)controller.getSessionAttr("currency"))){
				currency = controller.getSessionAttr("currency");
			}
			currency = config.get("config_currency");
			controller.setSessionAttr("currency", currency);
		}
		controller.setRoute(controller.getPara("route","common/home"));
		controller.setToken(controller.getPara("token"));
		controller.setPost("POST".equals(controller.getRequest().getMethod().toUpperCase()));
		controller.setCustomer((CustomerService)SessionConfigService.get("customer"));
		
		if(controller.getSessionAttr("shipping_address") != null){
			Address address = controller.getSessionAttr("shipping_address");
			tax.setShippingAddress(Integer.parseInt(""+address.get("country_id")), Integer.parseInt(""+address.get("zone_id")));
		}else if("shipping".equals(config.get("config_tax_default"))){
			tax.setShippingAddress(Integer.parseInt((String)config.get("config_country_id")),Integer.parseInt((String)config.get("config_zone_id")));
		}
		if(controller.getSessionAttr("payment_address") != null){
			Address address = controller.getSessionAttr("payment_address");
			tax.setPaymentAddress(Integer.parseInt(""+address.get("country_id")), Integer.parseInt(""+address.get("zone_id")));
		}else if("payment".equals(config.get("config_tax_default"))){
			tax.setPaymentAddress(Integer.parseInt((String)config.get("config_country_id")),Integer.parseInt((String)config.get("config_zone_id")));
		}
		tax.setPaymentAddress(Integer.parseInt((String)config.get("config_country_id")), 
				Integer.parseInt((String)config.get("config_zone_id")));
		tax.setStoreAddress(Integer.parseInt((String)config.get("config_country_id")), 
				Integer.parseInt((String)config.get("config_zone_id")));
		config.set("context_path", controller.getAttr("CONTEXT_PATH"));
		inv.invoke();
	}
}
