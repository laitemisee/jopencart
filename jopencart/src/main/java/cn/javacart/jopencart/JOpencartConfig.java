package cn.javacart.jopencart;

import javax.servlet.http.HttpServletRequest;

import cn.javacart.jfinal.php.render.PhpEnvPlugin;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jfinal.php.render.PhpRenderFactory;
import cn.javacart.jopencart.controller.catalog.account.AccountAccountController;
import cn.javacart.jopencart.controller.catalog.account.AccountLoginController;
import cn.javacart.jopencart.controller.catalog.account.AccountLogoutController;
import cn.javacart.jopencart.controller.catalog.common.CommonCurrencyModule;
import cn.javacart.jopencart.controller.catalog.common.CommonHomeController;
import cn.javacart.jopencart.controller.catalog.product.ProductProductController;
import cn.javacart.jopencart.handler.ImageScalrHandler;
import cn.javacart.jopencart.interceptor.JOpencartInitConfigInterceptor;
import cn.javacart.jopencart.interceptor.JOpencartThreadLocalResetInterceptor;
import cn.javacart.jopencart.library.LanguageService;
import cn.javacart.jopencart.model.*;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.alibaba.fastjson.JSON;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.SqlReporter;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.druid.IDruidStatViewAuth;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfplugin.kit.mockrender.MockRenderKit;

/**
 * 
 * @author farmer
 *
 */
public class JOpencartConfig extends JFinalConfig{

	@Override
	public void configConstant(Constants me) {
		loadPropertyFile("main_config.properties");//template
		PropKit.use("main_config.properties");
		me.setMainRenderFactory(new PhpRenderFactory());
		me.setDevMode(getPropertyToBoolean("devMode"));
	}

	@Override
	public void configRoute(Routes me) {
		me.add("/common/home",CommonHomeController.class);
		me.add("/common/currency",CommonCurrencyModule.class);
		me.add("/account/login", AccountLoginController.class);
		me.add("/account/logout",AccountLogoutController.class);
		me.add("/account/account", AccountAccountController.class);
		me.add("/product/product", ProductProductController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		me.add(new PhpEnvPlugin(getPropertyToBoolean("devMode"), PathKit.getWebRootPath().concat("/").concat(getProperty("template"))));
		DruidPlugin dp = new DruidPlugin(
				PropKit.use("db.properties").get("jdbcUrl"),
				PropKit.use("db.properties").get("user"), 
				PropKit.use("db.properties").get("password")
				);
		dp.set(10, 10, 50);
		dp.addFilter(new StatFilter());
		dp.addFilter(new WallFilter());
		ActiveRecordPlugin ap = new ActiveRecordPlugin(dp);
		me.add(dp);
		me.add(ap);
		ap.setShowSql(false);// 输出SQL
		SqlReporter.setLog(true);
		
		/*========系统默认数据库配置开始=======*/
		ap.addMapping("joc_address", "address_id",Address.class);
		ap.addMapping("joc_affiliate", "affiliate_id",Affiliate.class);
		ap.addMapping("joc_affiliate_activity", "affiliate_activity_id",AffiliateActivity.class);
		ap.addMapping("joc_affiliate_login", "affiliate_login_id",AffiliateLogin.class);
		ap.addMapping("joc_affiliate_transaction", "affiliate_transaction_id",AffiliateTransaction.class);
		ap.addMapping("joc_api", "api_id",Api.class);
		ap.addMapping("joc_api_ip", "api_ip_id",ApiIp.class);
		ap.addMapping("joc_api_session", "api_session_id",ApiSession.class);
		ap.addMapping("joc_attribute", "attribute_id",Attribute.class);
		ap.addMapping("joc_attribute_description", "attribute_id,language_id",AttributeDescription.class);
		ap.addMapping("joc_attribute_group", "attribute_group_id",AttributeGroup.class);
		ap.addMapping("joc_attribute_group_description", "attribute_group_id,language_id",AttributeGroupDescription.class);
		ap.addMapping("joc_banner", "banner_id",Banner.class);
		ap.addMapping("joc_banner_image", "banner_image_id",BannerImage.class);
		ap.addMapping("joc_banner_image_description", "banner_image_id,language_id",BannerImageDescription.class);
		ap.addMapping("joc_cart", "cart_id",Cart.class);
		ap.addMapping("joc_category", "category_id",Category.class);
		ap.addMapping("joc_category_description", "category_id,language_id",CategoryDescription.class);
		ap.addMapping("joc_category_filter", "category_id,filter_id",CategoryFilter.class);
		ap.addMapping("joc_category_path", "category_id,path_id",CategoryPath.class);
		ap.addMapping("joc_category_to_layout", "category_id,store_id",CategoryToLayout.class);
		ap.addMapping("joc_category_to_store", "category_id,store_id",CategoryToStore.class);
		ap.addMapping("joc_country", "country_id",Country.class);
		ap.addMapping("joc_coupon", "coupon_id",Coupon.class);
		ap.addMapping("joc_coupon_category", "coupon_id,category_id",CouponCategory.class);
		ap.addMapping("joc_coupon_history", "coupon_history_id",CouponHistory.class);
		ap.addMapping("joc_coupon_product", "coupon_product_id",CouponProduct.class);
		ap.addMapping("joc_currency", "currency_id",Currency.class);
		ap.addMapping("joc_custom_field", "custom_field_id",CustomField.class);
		ap.addMapping("joc_custom_field_customer_group", "custom_field_id,customer_group_id",CustomFieldCustomerGroup.class);
		ap.addMapping("joc_custom_field_description", "custom_field_id,language_id",CustomFieldDescription.class);
		ap.addMapping("joc_custom_field_value", "custom_field_value_id",CustomFieldValue.class);
		ap.addMapping("joc_custom_field_value_description", "custom_field_value_id,language_id",CustomFieldValueDescription.class);
		ap.addMapping("joc_customer", "customer_id",Customer.class);
		ap.addMapping("joc_customer_activity", "customer_activity_id",CustomerActivity.class);
		ap.addMapping("joc_customer_group", "customer_group_id",CustomerGroup.class);
		ap.addMapping("joc_customer_group_description", "customer_group_id,language_id",CustomerGroupDescription.class);
		ap.addMapping("joc_customer_history", "customer_history_id",CustomerHistory.class);
		ap.addMapping("joc_customer_ip", "customer_ip_id",CustomerIp.class);
		ap.addMapping("joc_customer_login", "customer_login_id",CustomerLogin.class);
		ap.addMapping("joc_customer_online", "ip",CustomerOnline.class);
		ap.addMapping("joc_customer_reward", "customer_reward_id",CustomerReward.class);
		ap.addMapping("joc_customer_transaction", "customer_transaction_id",CustomerTransaction.class);
		ap.addMapping("joc_customer_wishlist", "customer_id,product_id",CustomerWishlist.class);
		ap.addMapping("joc_download", "download_id",Download.class);
		ap.addMapping("joc_download_description", "download_id,language_id",DownloadDescription.class);
		ap.addMapping("joc_event", "event_id",Event.class);
		ap.addMapping("joc_extension", "extension_id",Extension.class);
		ap.addMapping("joc_filter", "filter_id",Filter.class);
		ap.addMapping("joc_filter_description", "filter_id,language_id",FilterDescription.class);
		ap.addMapping("joc_filter_group", "filter_group_id",FilterGroup.class);
		ap.addMapping("joc_filter_group_description", "filter_group_id,language_id",FilterGroupDescription.class);
		ap.addMapping("joc_geo_zone", "geo_zone_id",GeoZone.class);
		ap.addMapping("joc_information", "information_id",Information.class);
		ap.addMapping("joc_information_description", "information_id,language_id",InformationDescription.class);
		ap.addMapping("joc_information_to_layout", "information_id,store_id",InformationToLayout.class);
		ap.addMapping("joc_information_to_store", "information_id,store_id",InformationToStore.class);
		ap.addMapping("joc_language", "language_id",Language.class);
		ap.addMapping("joc_layout", "layout_id",Layout.class);
		ap.addMapping("joc_layout_module", "layout_module_id",LayoutModule.class);
		ap.addMapping("joc_layout_route", "layout_route_id",LayoutRoute.class);
		ap.addMapping("joc_length_class", "length_class_id",LengthClass.class);
		ap.addMapping("joc_length_class_description", "length_class_id,language_id",LengthClassDescription.class);
		ap.addMapping("joc_location", "location_id",Location.class);
		ap.addMapping("joc_manufacturer", "manufacturer_id",Manufacturer.class);
		ap.addMapping("joc_manufacturer_to_store", "manufacturer_id,store_id",ManufacturerToStore.class);
		ap.addMapping("joc_marketing", "marketing_id",Marketing.class);
		ap.addMapping("joc_modification", "modification_id",Modification.class);
		ap.addMapping("joc_module", "module_id",Module.class);
		ap.addMapping("joc_option", "option_id",Option.class);
		ap.addMapping("joc_option_description", "option_id,language_id",OptionDescription.class);
		ap.addMapping("joc_option_value", "option_value_id",OptionValue.class);
		ap.addMapping("joc_option_value_description", "option_value_id,language_id",OptionValueDescription.class);
		ap.addMapping("joc_order", "order_id",Order.class);
		ap.addMapping("joc_order_custom_field", "order_custom_field_id",OrderCustomField.class);
		ap.addMapping("joc_order_history", "order_history_id",OrderHistory.class);
		ap.addMapping("joc_order_option", "order_option_id",OrderOption.class);
		ap.addMapping("joc_order_product", "order_product_id",OrderProduct.class);
		ap.addMapping("joc_order_recurring", "order_recurring_id",OrderRecurring.class);
		ap.addMapping("joc_order_recurring_transaction", "order_recurring_transaction_id",OrderRecurringTransaction.class);
		ap.addMapping("joc_order_status", "order_status_id,language_id",OrderStatus.class);
		ap.addMapping("joc_order_total", "order_total_id",OrderTotal.class);
		ap.addMapping("joc_order_voucher", "order_voucher_id",OrderVoucher.class);
		ap.addMapping("joc_product", "product_id",Product.class);
		ap.addMapping("joc_product_attribute", "product_id,attribute_id,language_id",ProductAttribute.class);
		ap.addMapping("joc_product_description", "product_id,language_id",ProductDescription.class);
		ap.addMapping("joc_product_discount", "product_discount_id",ProductDiscount.class);
		ap.addMapping("joc_product_filter", "product_id,filter_id",ProductFilter.class);
		ap.addMapping("joc_product_image", "product_image_id",ProductImage.class);
		ap.addMapping("joc_product_option", "product_option_id",ProductOption.class);
		ap.addMapping("joc_product_option_value", "product_option_value_id",ProductOptionValue.class);
		ap.addMapping("joc_product_recurring", "product_id,recurring_id,customer_group_id",ProductRecurring.class);
		ap.addMapping("joc_product_related", "product_id,related_id",ProductRelated.class);
		ap.addMapping("joc_product_reward", "product_reward_id",ProductReward.class);
		ap.addMapping("joc_product_special", "product_special_id",ProductSpecial.class);
		ap.addMapping("joc_product_to_category", "product_id,category_id",ProductToCategory.class);
		ap.addMapping("joc_product_to_download", "product_id,download_id",ProductToDownload.class);
		ap.addMapping("joc_product_to_layout", "product_id,store_id",ProductToLayout.class);
		ap.addMapping("joc_product_to_store", "product_id,store_id",ProductToStore.class);
		ap.addMapping("joc_recurring", "recurring_id",Recurring.class);
		ap.addMapping("joc_recurring_description", "recurring_id,language_id",RecurringDescription.class);
		ap.addMapping("joc_return", "return_id",Return.class);
		ap.addMapping("joc_return_action", "return_action_id,language_id",ReturnAction.class);
		ap.addMapping("joc_return_history", "return_history_id",ReturnHistory.class);
		ap.addMapping("joc_return_reason", "return_reason_id,language_id",ReturnReason.class);
		ap.addMapping("joc_return_status", "return_status_id,language_id",ReturnStatus.class);
		ap.addMapping("joc_review", "review_id",Review.class);
		ap.addMapping("joc_setting", "setting_id",Setting.class);
		ap.addMapping("joc_stock_status", "stock_status_id,language_id",StockStatus.class);
		ap.addMapping("joc_store", "store_id",Store.class);
		ap.addMapping("joc_tax_class", "tax_class_id",TaxClass.class);
		ap.addMapping("joc_tax_rate", "tax_rate_id",TaxRate.class);
		ap.addMapping("joc_tax_rate_to_customer_group", "tax_rate_id,customer_group_id",TaxRateToCustomerGroup.class);
		ap.addMapping("joc_tax_rule", "tax_rule_id",TaxRule.class);
		ap.addMapping("joc_upload", "upload_id",Upload.class);
		ap.addMapping("joc_url_alias", "url_alias_id",UrlAlias.class);
		ap.addMapping("joc_user", "user_id",User.class);
		ap.addMapping("joc_user_group", "user_group_id",UserGroup.class);
		ap.addMapping("joc_voucher", "voucher_id",Voucher.class);
		ap.addMapping("joc_voucher_history", "voucher_history_id",VoucherHistory.class);
		ap.addMapping("joc_voucher_theme", "voucher_theme_id",VoucherTheme.class);
		ap.addMapping("joc_voucher_theme_description", "voucher_theme_id,language_id",VoucherThemeDescription.class);
		ap.addMapping("joc_weight_class", "weight_class_id",WeightClass.class);
		ap.addMapping("joc_weight_class_description", "weight_class_id,language_id",WeightClassDescription.class);
		ap.addMapping("joc_zone", "zone_id",Zone.class);
		ap.addMapping("joc_zone_to_geo_zone", "zone_to_geo_zone_id",ZoneToGeoZone.class);
		/*========系统默认数据库配置结束=======*/
		
		me.add(new EhCachePlugin());
	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new JOpencartInitConfigInterceptor());	//初始化Opencart相关
		me.add(new JOpencartThreadLocalResetInterceptor());	//重置线程变量
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new ImageScalrHandler());
		me.add(new ContextPathHandler());
		me.add(new DruidStatViewHandler("/druid", new IDruidStatViewAuth() {
			@Override
			public boolean isPermitted(HttpServletRequest request) {
				return true;
			}
		}));
	}

	@Override
	public void afterJFinalStart() {
		LogKit.info(MockRenderKit.render(new PhpRender("/start.php")));
		JSON.parse("{}");
		LanguageService.getInstance().loadDefault("/catalog/language/en-gb/en-gb.php");	//初始化加载语言
		super.afterJFinalStart();
	}
	
	@Override
	public void beforeJFinalStop() {
		super.beforeJFinalStop();
	}
	
}
