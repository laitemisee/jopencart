package cn.javacart.jopencart.library;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.kit.StrKit;

import cn.javacart.jopencart.model.Setting;

/**
 * 全局配置
 * @author farmer
 *
 */
public class ConfigService{

	private static ConfigService configService = null;
	
	private static Map<String, Object> configMap = new HashMap<String, Object>();
	
	private ConfigService() {

	}

	public static synchronized ConfigService getInstance() {
		if (configService == null) {
			configService = new ConfigService();
			init();
		}
		return configService;
	}
	
	static void init(){
		List<Setting> settings = Setting.ME.find("select t.* from joc_setting t");
		for (Setting setting : settings) {
			configMap.put(setting.getStr("key"), setting.getStr("value"));
		}
	}
	
	public void set(String key ,Object value){
		configMap.put(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T get(String key){
		return (T) configMap.get(key);
	}

	/**
	 * 是否设置
	 * @param key
	 * @return
	 */
	public boolean isset(String key){
		return StrKit.notBlank((String)get("config_tax")) &&  !"0".equals(get("config_tax"));
	}
	
}
