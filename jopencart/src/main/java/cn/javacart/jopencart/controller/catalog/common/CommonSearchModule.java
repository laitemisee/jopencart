package cn.javacart.jopencart.controller.catalog.common;

import php.runtime.memory.support.MemoryUtils;

import com.jfplugin.kit.mockrender.MockRenderKit;

import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;

/**
 * 语言组件
 * @author farmer
 *
 */
public class CommonSearchModule extends JOpencartModule{

	public CommonSearchModule(JOpencartController controller) {
		super(controller);
	}

	@Override
	public String render() {
		language.load("/catalog/language/{0}/common/language.php");
		dataMap.put("text_search", language.get("text_search"));
		dataMap.put("search", MemoryUtils.valueOf(controller.getPara("search","")));
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/common/search.tpl", dataMap));
	}

}
