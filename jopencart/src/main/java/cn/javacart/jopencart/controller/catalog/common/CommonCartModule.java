package cn.javacart.jopencart.controller.catalog.common;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;

import com.jfplugin.kit.mockrender.MockRenderKit;

/**
 * 语言组件
 * @author farmer
 *
 */
public class CommonCartModule extends JOpencartModule{

	public CommonCartModule(JOpencartController controller) {
		super(controller);
	}

	@Override
	public String render() {
		language.load("/catalog/language/{0}/common/cart.php");
		dataMap.put("text_empty", language.get("text_empty"));
		dataMap.put("text_cart", language.get("text_cart"));
		dataMap.put("text_checkout", language.get("text_checkout"));
		dataMap.put("text_recurring", language.get("text_recurring"));
		dataMap.put("text_items", MemoryUtils.valueOf(String.format(language.getStr("text_items"),0,"$0.00")));
		dataMap.put("text_loading", language.get("text_loading"));
		dataMap.put("button_remove", language.get("button_remove"));
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/common/cart.tpl", dataMap));
	}

}
